#!/bin/bash 

#set -ueo pipefail
#set -ue
IFS=$'\n\t'


# remove text from all files
#for i in *; do cd ${i};$(for x in *; do mv "${x}" "${x#'[9anime.to] '}"; done); cd ..;done

# no IFS voodoo
# for f in *\ *; do mv "$f" "${f// /_}"; done

# find dir, cd into it, remove text from filename, cd ..
for i in $(find . -type d); do cd ${i};$(for x in *; do mv "${x}" "${x#'[9anime.to] '}"; done); cd - ;done
#for i in *; do echo cd ${i};$(for x in *; do mv "${x}" "${x#'[9anime.to]_'}"; done);echo cd ..;done

# find all dirs and get rid off whitespace
for i in $(find . -type d); do mv "${i}" $(echo "${i}" | tr " " _); done

# find all files and get rid off whitespace
for i in $(find . -type f); do mv "${i}" $(echo "${i}" | tr " " _); done


exit 0
