#!/bin/bash

# Version: 2021.06.09

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob

export SGDISK="$(which sgdisk)"
export PARTD="$(which parted)"
export SDCARD=$1
export USB_STICK=$2
export MNT_ROOT="/mnt"
export MNT_BOOT="/mnt/boot"


prep_storage(){
  ${SGDISK} -Z /dev/${SDCARD}
  ${SGDISK} -Z /dev/${USB_STICK}

  ${PARTD} -s /dev/${SDCARD} mklabel msdos
  ${PARTD} -s -a optimal -- /dev/${SDCARD} mkpart primary fat32 0% 512M
  #${PARTD} -s -a optimal -- /dev/${SDCARD} name 1 boot
  ${PARTD} -s /dev/${SDCARD} set 1 boot on
  mkfs.vfat /dev/${SDCARD}1

  ${PARTD} -s /dev/${USB_STICK} mklabel msdos
  ${PARTD} -s -a optimal -- /dev/${USB_STICK} mkpart primary ext4 0% 10G
  #${PARTD} -s -a optimal -- /dev/${USB_STICK} name 1 root
  mkfs.ext4 -FL root /dev/${USB_STICK}1
}

mount_and_extract(){
  mount /dev/${USB_STICK}1 ${MNT_ROOT}
  mkdir -p ${MNT_BOOT}
  mount /dev/${SDCARD}1 ${MNT_BOOT}

  echo "give full path of tar.gz image:"
  read TGZ_IMG
  bsdtar -xpf ${TGZ_IMG} -C ${MNT_ROOT}
}

adjust_fstab(){
  export ROOT_UUID="$(blkid /dev/${USB_STICK}1 -s UUID -o value)"
  export BOOT_UUID="$(blkid /dev/${SDCARD}1 -s UUID -o value)"

  echo "UUID=${ROOT_UUID}	/         	ext4     	defaults,data=ordered	0 1" >> ${MNT_ROOT}/etc/fstab
  echo "UUID=${BOOT_UUID}	/boot      	vfat     	defaults	0 2" >> ${MNT_ROOT}/etc/fstab
}





# run functions
prep_storage
mount_and_extract
adjust_fstab


exit 0