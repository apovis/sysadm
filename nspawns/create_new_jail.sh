#!/bin/bash

# Version: 2023.04.17

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
#set -x

read_jails_name(){
  echo "what is your jail's name:"
  read JAIL_NAME
  export JAIL_NAME
}

copy_template_2_jail(){
  cp template_systemd_nspawn /etc/systemd/nspawn/${JAIL_NAME}.nspawn
  sed -i "s/HOSTNAME_2_BE_CHANGED/${JAIL_NAME}/g" "/etc/systemd/nspawn/${JAIL_NAME}.nspawn"
}





# run funcs here
read_jails_name
copy_template_2_jail