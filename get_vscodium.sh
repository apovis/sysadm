#!/usr/bin/env bash

# Version: 2024.11.11

# get latest vscodium version from git

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
#set -x

export TAG_LATEST="$(curl -s https://api.github.com/repos/VSCodium/vscodium/releases/latest | jq -r '.tag_name')"
export VSC_DIR="${HOME}/.local/opt/${TAG_LATEST}"
export VSC_SLNK="${HOME}/.local/opt/vscodium"
export VSC_FILE="VSCodium-linux-x64-${TAG_LATEST}.tar.gz"
export DOWNLOADS_TEMP="$(mktemp -d /tmp/downloadsXXXXX)"

if [[ -d "${VSC_DIR}" ]] && [[ "${VSC_DIR##*/}" == "${TAG_LATEST}" ]]
then
  echo "already newest version in place"
  exit 0
elif [[ -L ${VSC_SLNK} ]] && [[ -d "${VSC_DIR}" ]]
then
  unlink ${VSC_SLNK}
  rm -rf "${VSC_DIR}"
  mkdir -p ${VSC_DIR}
else
  mkdir -p ${VSC_DIR}
fi

# get latest tar.gz
cd ${DOWNLOADS_TEMP}
curl -L --remote-name-all $(curl -s https://api.github.com/repos/VSCodium/vscodium/releases/latest | \
jq -r '.assets[] | select(.name | test("VSCodium-linux-x64-.*\\.tar\\.gz")) | .browser_download_url' | xargs)

# check sha256sums
sha256sum --status -c ${VSC_FILE}.sha256

# extract
if [[ $? = 0 ]]
then
  tar xzf ${VSC_FILE} -C ${VSC_DIR}
  ln -sf ${VSC_DIR} ${VSC_SLNK}
else
  echo "sha256sums do not match !!!"
  exit 1
fi

# clean up
rm -rf ${DOWNLOADS_TEMP}

exit 0
