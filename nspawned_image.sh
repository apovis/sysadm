#!/usr/bin/env bash

# Version: 2024.04.02
set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
set -x

export IMAGE_NAME=$1
export IMAGE_SIZE=$2
export DISTRO=$3

export FALLOCATE="$(which fallocate)"
export LOSETUP="$(which losetup)"
export IMAGE_PATH="/var/lib/machines"

export CHROOT_MOUNT_POINT="/mnt"

export SYSTEMD_NSPAWN_CONFIG_DIR="/etc/systemd/nspawn"


check_for_parameters() {
  if [[ $# -lt 3 ]] && [[ $# -gt 3 ]]
  then
    echo "No or too many arguments supplied"
    echo "usage: ./$0 IMAGE_NAME IMAGE_SIZE DISTRO"
    echo "./$0 rocky9 20G rocky"
    exit 1
  fi
}

create_image() {
  ${FALLOCATE} -l ${IMAGE_SIZE} ${IMAGE_PATH}/${IMAGE_NAME}
}

format_image() {
  mkfs.btrfs -fL ossys ${IMAGE_PATH}/${IMAGE_NAME}
}

bind_to_loop() {
  ${LOSETUP} -f ${IMAGE_PATH}/${IMAGE_NAME}
}

get_loop_dev() {
  export LOOP_DEV="$(losetup -a | grep -i ${IMAGE_PATH}/${IMAGE_NAME} | awk -F ":" '{print $1}')"
}

mount_loop() {
  mount ${LOOP_DEV} ${CHROOT_MOUNT_POINT}
  df -HT ${CHROOT_MOUNT_POINT}
}

install_distro() {
  case ${DISTRO} in 

  arch)
    pacstrap -i -c ${CHROOT_MOUNT_POINT} base base-devel tmux neovim curl git tig rsync --ignore linux
    echo "pts/0" >> ${CHROOT_MOUNT_POINT}/etc/securetty
  ;;

  debian)
    debootstrap --arch=amd64 --variant=minbase --include=systemd,dbus bullseye ${CHROOT_MOUNT_POINT} http://ftp.de.debian.org/debian/
  ;;

  fedora)
    dnf --releasever=40 --best --setopt=install_weak_deps=False --repo=fedora --repo=updates --installroot=${CHROOT_MOUNT_POINT} install -y dhcp-client dnf fedora-release glibc glibc-langpack-en iputils less ncurses passwd systemd systemd-networkd systemd-resolved util-linux shadow libpwquality 
    ;;

  rocky)
    dnf --repo=rocky-base --repo=rocky-apps --repo=rocky-extras --releasever=9 --best --installroot=${CHROOT_MOUNT_POINT} --setopt=install_weak_deps=False install -y @minimal-environment epel-release elrepo-release git curl tmux  bash-completion shadow libpwquality
  ;;

  ubuntu)
    debootstrap --variant=minbase --arch=amd64 jammy ${CHROOT_MOUNT_POINT} http://de.archive.ubuntu.com/ubuntu
  ;;

  *)
    echo "supported types are:
    - arch
    - debian
    - fedora
    - rocky
    - ubuntu"
    exit 1
  ;;

  esac
}

set_root_passwd() {
  chroot ${CHROOT_MOUNT_POINT} /bin/bash -c "passwd root"
}

copy_template_4_jail() {
  if [[ -d ${SYSTEMD_NSPAWN_CONFIG_DIR} ]]
  then
    echo "directory ${SYSTEMD_NSPAWN_CONFIG_DIR} already exists"
  else
    mkdir -p ${SYSTEMD_NSPAWN_CONFIG_DIR}
  fi

  cp nspawns/template_systemd_nspawn ${SYSTEMD_NSPAWN_CONFIG_DIR}/${IMAGE_NAME}.nspawn
  sed -i "s/HOSTNAME_2_BE_CHANGED/${IMAGE_NAME}/g" "${SYSTEMD_NSPAWN_CONFIG_DIR}/${IMAGE_NAME}.nspawn"

  systemctl daemon-reload
}

umount_loops_and_detach_them() {
  umount ${CHROOT_MOUNT_POINT}
  losetup -D
  losetup -a
}

list_machines_status() {
  systemctl status systemd-nspawn@${IMAGE_NAME}.service
}


# run functions
create_image
format_image
bind_to_loop
get_loop_dev
mount_loop
install_distro
set_root_passwd
copy_template_4_jail
umount_loops_and_detach_them
list_machines_status


exit 0
