#!/usr/bin/env bash

# Version: 2024.10.28

# get latest brave version from git

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
#set -x

export TAG_LATEST_VERSION="$(curl -s https://api.github.com/repos/brave/brave-browser/releases/latest | jq -r '.tag_name')"
export TAG_LATEST="${TAG_LATEST_VERSION#*v}"
export BRAVE_DIR="${HOME}/.local/opt/${TAG_LATEST}"
export BRAVE_SLNK="${HOME}/.local/opt/brave"
export BRAVE_FILE="brave-browser-${TAG_LATEST}-linux-amd64.zip"

if [[ -d "${BRAVE_DIR}" ]] && [[ "${BRAVE_DIR##*/}" == "${TAG_LATEST}" ]]
then
  echo "already newest version in place"
  exit 0
elif [[ -L ${BRAVE_SLNK} ]] && [[ -d "${BRAVE_DIR}" ]]
then
  unlink ${BRAVE_SLNK}
  rm -rf "${BRAVE_DIR}"
  mkdir -p ${BRAVE_DIR}
  cd ${BRAVE_DIR}
else
  mkdir -p ${BRAVE_DIR}
  cd ${BRAVE_DIR}
fi

# get latest tar.gz
curl -L --remote-name-all $(curl -s https://api.github.com/repos/brave/brave-browser/releases/latest | jq -r '.assets[] | select(.name | test(".*-linux-amd64\\.zip")) | .browser_download_url' | xargs)

# check sha256sums
sha256sum --status -c ${BRAVE_FILE}.sha256

# extract
if [[ $? = 0 ]]
then
  7z x ${BRAVE_FILE}
  ln -s ${BRAVE_DIR} ${BRAVE_SLNK}
else
  echo "sha256sums do not match !!!"
  exit 1
fi

# clean up
rm *.zip *.sha256 *.asc

exit 0
