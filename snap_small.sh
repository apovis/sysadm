#!/bin/bash
# Version: 2022.06.03
# backup opt var boot

set -o nounset
set -o errexit
set -o noclobber
set -o noglob

eval $(date +"year=%Y month=%m day=%d hour=%H minute=%M second=%S")


tar cvf /.snapshots/${year}_${month}_${day}_-_${hour}.${minute}.${second}_boot_efi.tar /boot/efi

for i in opt var boot ; do btrfs subvolume snapshot -r /${i} /.snapshots/${year}_${month}_${day}_-_${hour}.${minute}.${second}_${i} ;done

exit 0