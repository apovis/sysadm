#!/usr/bin/env bash

# Version: 2024.08.01
set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
#set -x

export IMAGE_NAME=$1
export IMAGE_SIZE=$2

export FALLOCATE="$(which fallocate)"
export LOSETUP="$(which losetup)"
#export IMAGE_PATH="/var/lib/machines"
export IMAGE_PATH="/nspawns_oleg"

export MOUNT_POINT="/mnt"

create_image() {
  ${FALLOCATE} -l ${IMAGE_SIZE} ${IMAGE_PATH}/${IMAGE_NAME}
}

format_image() {
  mkfs.btrfs -fL ossys ${IMAGE_PATH}/${IMAGE_NAME}
}

bind_to_loop() {
  ${LOSETUP} -f ${IMAGE_PATH}/${IMAGE_NAME} 
}

get_loop_dev() {
  export LOOP_DEV="$(losetup -a | grep -i ${IMAGE_PATH}/${IMAGE_NAME} | awk -F ":" '{print $1}')"
}

mount_loop() {
  mount ${LOOP_DEV} ${MOUNT_POINT}
  df -HT ${MOUNT_POINT}
}




# run functions
create_image
format_image
bind_to_loop
get_loop_dev
mount_loop

exit 0
