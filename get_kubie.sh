#!/usr/bin/env bash

# Version: 2024.10.27

# get latest kubie version from git

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
#set -x

export GIT_API_URL="https://api.github.com/repos/sbstp/kubie/releases/latest"
export BIN_DIR="/usr/local/bin"
export BIN_FILE="kubie-linux-amd64"
export DOWNLOADS_TEMP="$(mktemp -d /tmp/downloadsXXXXX)"

# get latest tar.gz
cd ${DOWNLOADS_TEMP}
curl -LO $(curl -s "${GIT_API_URL}" | jq -r '.assets[] | select(.name | test("kubie-linux-amd64")) | .browser_download_url')

# install
doas install -o root -g root -m755 ${BIN_FILE} ${BIN_DIR}/kubie

# clean up
rm -rf ${DOWNLOADS_TEMP}

exit 0