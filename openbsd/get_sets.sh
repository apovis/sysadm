#!/usr/bin/env bash

# Version: 2024.05.06
set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
#set -x

ARCH="amd64"
OBSD_VERSION="7.5"
DOWNLOADS="/data/downloads/sets"
SETS_URL="//cdn.openbsd.org/pub/OpenBSD/${OBSD_VERSION}/${ARCH}/"
WGET="$(which wget)"
WGET_OPTS="--no-verbose --no-parent --recursive --level=1 --no-directories -A .tgz"

mkdir -p ${DOWNLOADS}
cd ${DOWNLOADS}
${WGET} ${WGET_OPTS} ${SETS_URL}

echo "done"

ls -lah ${DOWNLOADS}