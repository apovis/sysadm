#!/usr/bin/env bash

# Version: 2024.05.06
set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
#set -x

SRC_CHROOT="/data/src_chroot_01"
SETS_DIR="/data/downloads/sets"

mkdir -p ${SRC_CHROOT}
for i in $(ls ${SETS_DIR}/*.tgz|xargs); do echo "extracting ${i}";tar -xzphf ${i} -C ${SRC_CHROOT};done

echo "finished extracting sets"

ls -lah ${SRC_CHROOT}