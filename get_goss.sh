#!/usr/bin/env bash

# Version: 2024.11.12

# get latest kubie version from git

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
set -x

export GIT_API_URL="https://api.github.com/repos/goss-org/goss/releases/latest"
export BIN_DIR="/usr/local/bin"
export BIN_FILE="goss-linux-amd64"
export DOWNLOADS_TEMP="$(mktemp -d /tmp/downloadsXXXXX)"

# get latest release
cd ${DOWNLOADS_TEMP}
curl -LO $(curl -s "${GIT_API_URL}" | jq -r '.assets[] | select(.name | test("goss-linux-amd64")) | .browser_download_url')

# install
doas install -o root -g root -m755 ${BIN_FILE} ${BIN_DIR}/goss

# clean up
rm -rf ${DOWNLOADS_TEMP}

exit 0
