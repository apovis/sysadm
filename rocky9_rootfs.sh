#!/bin/bash

# purpose: 
# create a minimal rocky 9 rootfs tarball

# Version: 2024.03.29
set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
set -x

# define some vars
export DNF="$(which dnf)"
export REPO_OPTS="--repo=rocky-base --repo=rocky-apps --repo=rocky-extras --releasever=9 --best --setopt install_weak_deps=false --nodocs -y"
export INSTALL_ROOT="/containers/apovis_storage/CHROT"
export PKGS_2_INSTALL="bash coreutils-single curl-minimal glibc-minimal-langpack libcurl-minimal libusbx microdnf rocky-release rootfiles"
export PKGS_2_REMOVE="brotli dosfstools e2fsprogs firewalld fuse-libs gettext* gnupg2-smime grub\* hostname iptables iputils kernel kexec-tools less libss os-prober* pinentry qemu-guest-agent rootfiles shared-mime-info tar trousers vim-minimal xfsprogs xkeyboard-config yum"
# rpm dnf-data xz-libs gmp  grep sed gnupg2 microdnf
export ROOTFS_TAR_DIR="/containers/apovis_storage/"
export PODMAN="$(which podman)"

# create install_root dir
make_install_root_dir() {
  if [[ -d "${INSTALL_ROOT}" ]]
  then
    doas rm -rf "${INSTALL_ROOT}"
  else
    mkdir -p ${INSTALL_ROOT}
  fi
}

install_into_chroot() {
  doas ${DNF} ${REPO_OPTS} --installroot ${INSTALL_ROOT} install ${PKGS_2_INSTALL}
  doas ${DNF} -y --installroot ${INSTALL_ROOT} remove ${PKGS_2_REMOVE}
  doas ${DNF} --installroot ${INSTALL_ROOT} clean all
  doas rm -rf ${INSTALL_ROOT}/var/cache/* ${INSTALL_ROOT}/var/log/dnf* ${INSTALL_ROOT}/usr/share/zoneinfo ${INSTALL_ROOT}/etc/udev/hwdb.bin ${INSTALL_ROOT}/usr/lib/udev/hwdb.d/  ${INSTALL_ROOT}/boot ${INSTALL_ROOT}/var/lib/dnf/history.* "${INSTALL_ROOT}/tmp/*" "${INSTALL_ROOT}/tmp/.*"
}

make_tar_from_rootfs(){
  cd "${INSTALL_ROOT%/*}"
  doas tar cJf ${ROOTFS_TAR_DIR}/$(date -I)_rocky9.tar.xz "${INSTALL_ROOT##*/}"
}

podman_import_rootfs_tar(){
  ${PODMAN} import --change "CMD /bin/bash" ${ROOTFS_TAR_DIR}/$(date -I)_rocky9.tar.xz rocky9small
  podman images
}

# run functions here
make_install_root_dir
install_into_chroot
make_tar_from_rootfs
podman_import_rootfs_tar

exit 0
