#!/bin/bash -x
# Version: 2022.06.03
# backup just root and home

set -o nounset
set -o errexit
set -o noclobber
set -o noglob

eval $(date +"year=%Y month=%m day=%d hour=%H minute=%M second=%S")

btrfs subvolume snapshot -r / /.snapshots/${year}_${month}_${day}_-_${hour}.${minute}.${second}_root
btrfs subvolume snapshot -r /home /.snapshots/${year}_${month}_${day}_-_${hour}.${minute}.${second}_home

exit 0