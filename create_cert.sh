#!/bin/bash

#
# last modified 2024.05.24
# aleg@gmx.at
#

set -o nounset
set -o errexit
#set -o noclobber
set -o noglob

CERTS_DIR="$(pwd)/CERTS_$(date -I)"
mkdir -p ${CERTS_DIR}

# create a config file for openssl
CONFIG=$(mktemp -q /tmp/openssl-conf.XXXXXXXX)
if [ ! $? -eq 0 ]; then
    echo "Could not create temporary config file. exiting"
    exit 1
fi

echo "Private Key and Certificate Signing Request Generator"
echo

read -p "FQDN/CommonName (ie. Oleg_Lusan): " COMMONNAME
read -p "OU (leave blank for 'Server Operations'): " ORGUNIT
ORGUNIT=${ORGUNIT:'Server Operations'}
read -p "Key Type (leave blank for 'ecdsa-p512', or 'rsa', 'ecdsa', 'ecdsa-p384', 'ecdsa-p512'): " KEYTYPE
KEYTYPE=${KEYTYPE:-ecdsa-p512}

echo "Type SubjectAltNames for the certificate, one per line. Enter a blank line to finish"
SAN=1        # bogus value to begin the loop
SANAMES=""   # sanitize
while [ ! "${SAN}" = "" ]; do
    printf "SubjectAltName: DNS:"
    read SAN
    if [ "${SAN}" = "" ]; then break; fi # end of input
    if [ "${SANAMES}" = "" ]; then
        SANAMES="DNS:${SAN}"
    else
        SANAMES="${SANAMES},DNS:${SAN}"
    fi
done

# Config File Generation

cat <<EOF > ${CONFIG}
# -------------- BEGIN custom openssl.cnf -----
 HOME                    = ${HOME}
EOF

cat <<EOF >> ${CONFIG}
 oid_section             = new_oids
 [ new_oids ]
 [ req ]
 default_md              = sha512
 default_days            = 1170 			# 39 months
 default_keyfile         = ${COMMONNAME}-$(date +%Y%m%d).key
 distinguished_name      = req_distinguished_name
 encrypt_key             = no
 string_mask = nombstr
EOF

if [ ! "${SANAMES}" = "" ]; then
    echo "req_extensions = v3_req # Extensions to add to certificate request" >> ${CONFIG}
fi

cat <<EOF >> $CONFIG
 [ req_distinguished_name ]
 countryName                     = Country Name (2 letter code)
 countryName_default             = DE
 countryName_min                 = 2
 countryName_max                 = 2
 stateOrProvinceName             = State or Province Name (full name)
 stateOrProvinceName_default     = Hessen
 localityName                    = Locality Name (eg, city)
 localityName_default            = FFM
 0.organizationName              = Organization Name (eg, company)
 0.organizationName_default      = DreamingVoid
 organizationalUnitName          = Organizational Unit Name (eg, section)
 organizationalUnitName_default  = ${ORGUNIT}
 commonName                      = Common Name (eg, YOUR name)
 commonName_default              = ${COMMONNAME}
 commonName_max                  = 64
 emailAddress                    = Email Address
 emailAddress_default            = your@mail.here
 emailAddress_max                = 40
 [ v3_req ]
 nsCertType                      = server
 basicConstraints                = critical,CA:false
EOF

if [ ! "${SANAMES}" = "" ]; then
    echo "subjectAltName=${SANAMES}" >> ${CONFIG}
fi

echo "# -------------- END custom openssl.cnf -----" >> ${CONFIG}

echo "Running OpenSSL..."

SETSHA="-sha512"
case "${KEYTYPE}" in
"ecdsa")
	openssl ecparam -out ${CERTS_DIR}/${COMMONNAME}-$(date +%Y%m%d).key -name prime256v1 -genkey
    ;;
"ecdsa-p384")
	openssl ecparam -out ${CERTS_DIR}/${COMMONNAME}-$(date +%Y%m%d).key -name secp384r1 -genkey
    setsha="-sha384"
    ;;
"ecdsa-p512")
	openssl ecparam -out ${CERTS_DIR}/${COMMONNAME}-$(date +%Y%m%d).key -name secp521r1 -genkey
    setsha="-sha512"
    ;;
*)
	openssl genrsa -out ${CERTS_DIR}/${COMMONNAME}-$(date +%Y%m%d).key 2048
    ;;
esac
openssl req -batch -config ${CONFIG} -new -key ${CERTS_DIR}/${COMMONNAME}-$(date +%Y%m%d).key -out ${CERTS_DIR}/${COMMONNAME}-$(date +%Y%m%d).csr ${SETSHA}

echo "Review and send the Certificate Signing Request below to Digicert."
echo
echo -------------------------------------------------------------------------
openssl req -in ${CERTS_DIR}/${COMMONNAME}-$(date +%Y%m%d).csr -text
echo -------------------------------------------------------------------------
echo
echo The Certificate request is also available in ${CERTS_DIR}/${COMMONNAME}-$(date +%Y%m%d).csr
echo The Private Key is stored in ${CERTS_DIR}/${COMMONNAME}-$(date +%Y%m%d).key
echo

#if [ -z "$AWS_SESSION_TOKEN" ]; then
#    echo No AWS session found, skipping encryption of the private key.
#    echo Make sure to encrypt it with SOPS using
#    echo $ ../encrypt_prod_file.sh ${COMMONNAME}-$(date +%Y%m%d).key
#else
#    echo Encrypting the private key with SOPS and committing it to git.
#    ../encrypt_prod_file.sh ${COMMONNAME}-$(date +%Y%m%d).key && git commit -m "New private key for ${COMMONNAME}" ${COMMONNAME}-$(date +%Y%m%d).key
#fi

rm $CONFIG
