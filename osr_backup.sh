#!/bin/bash

# Version: 2022.07.07
set -o nounset
set -o errexit
set -o noclobber
#set -o noglob
#set -x

# https://help.ubuntu.com/community/EOLUpgrades/

PARTC_XFS="$(which partclone.xfs)"
PARTC_VFAT="$(which partclone.vfat)"
VGCHANGE="$(which vgchange)"
SFDISK="$(which sfdisk)"
VG_GROUP="ubuntu-vg"
FULL_DEV="/dev/ubuntu-vg"

docker  home  netdata_logs  opt  root  srv  swap  usr  var  varlog

activate_vg() {
    ${VGCHANGE} -ay
}

create_mount_backup() {
    mkdir /backup
    mount ${FULL_DEV}/backups_oleg /backup
}

get_etc() {
    tar cvJf /etc /backup/$(date -I)_etc.tar.xz
}

create_mbr_gpt_backup() {
    ${SFDISK} -d /dev/sda > /backup/$(date -I)_sda.sfdisk.dump
}

backup_efi() {
    ${PARTC_VFAT} -c -L /backup/$(date -I)_efi.log -s /dev/sda1 -o /backup/$(date -I)_efi_536MB.vfat 
}

backup_xfses() {
    ${PARTC_XFS} -c -L /backup/$(date -I)_boot.log -s ${FULL_DEV}/boot -o /backup/$(date -I)_boot_1GB.xfs
    ${PARTC_XFS} -c -L /backup/$(date -I)_root.log -s ${FULL_DEV}/root -o /backup/$(date -I)_root_20GB.xfs
    ${PARTC_XFS} -c -L /backup/$(date -I)_opt.log -s ${FULL_DEV}/opt -o /backup/$(date -I)_opt_11GB.xfs
    ${PARTC_XFS} -c -L /backup/$(date -I)_home.log -s ${FULL_DEV}/home -o /backup/$(date -I)_home_11GB.xfs
    ${PARTC_XFS} -c -L /backup/$(date -I)_srv.log -s ${FULL_DEV}/srv -o /backup/$(date -I)_srv_215GB.xfs
    ${PARTC_XFS} -c -L /backup/$(date -I)_netdata.log -s ${FULL_DEV}/netdata -o /backup/$(date -I)_netdata_33GB.xfs
    ${PARTC_XFS} -c -L /backup/$(date -I)_docker.log -s ${FULL_DEV}/docker -o /backup/$(date -I)_docker_108GB.xfs
    ${PARTC_XFS} -c -L /backup/$(date -I)_var.log -s ${FULL_DEV}/var -o /backup/$(date -I)_var_11GB.xfs
    ${PARTC_XFS} -c -L /backup/$(date -I)_varlog.log -s ${FULL_DEV}/varlog -o /backup/$(date -I)_varlog_11GB.xfs
    ${PARTC_XFS} -c -L /backup/$(date -I)_usr.log -s ${FULL_DEV}/usr -o /backup/$(date -I)_usr_110G.xfs
}


# run functions
activate_vg
create_mount_backup
get_etc
create_mbr_gpt_backup
backup_efi
backup_xfses


exit 0