#!/usr/bin/env bash

# Version: 2024.05.18
set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
#set -x


# build iso, glibc iso supports also muslc
time ./mklive.sh -a x86_64 -k us -l en_US.UTF-8 -i xz -s xz -o void-$(date +"%Y%m%d%H%M")_glibc.iso -K -c ./cache -p "linux6.8 linux6.8-headers tmux neovim git gptfdisk zfs bash htop curl iproute2 inetutils parted void-repo-nonfree xtools iwd rsync"

# create rootfs for x86_64
time ./mkrootfs.sh -b base-container -o void-$(date +"%Y%m%d%H%M")_x86_64_base_musl.rootfs x86_64-musl
time ./mkrootfs.sh -b base-container -o void-$(date +"%Y%m%d%H%M")_x86_64_base.rootfs x86_64
time ./mkrootfs.sh -o void-$(date +"%Y%m%d%H%M")_x86_64_full.rootfs x86_64
time ./mkrootfs.sh -o void-$(date +"%Y%m%d%H%M")_x86_64_full_musl.rootfs x86_64-musl

# create rootfs for aarch64
time ./mkrootfs.sh -o void-$(date +"%Y%m%d%H%M")_aarch64_musl_full.rootfs aarch64-musl
time ./mkrootfs.sh -o void-$(date +"%Y%m%d%H%M")_aarch64_full.rootfs aarch64
time ./mkrootfs.sh -b base-container -o void-$(date +"%Y%m%d%H%M")_aarch64_musl_base.rootfs aarch64-musl
time ./mkrootfs.sh -b base-container -o void-$(date +"%Y%m%d%H%M")_aarch64_base.rootfs aarch64

exit 0