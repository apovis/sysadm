#!/bin/bash

# Version: 2023.11.30
# setup ubuntu repo for docker

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
set -x

export UBUNTU_VERSION="jammy"
export URL_2_MIRROR="https://download.docker.com/linux/ubuntu/dists/${UBUNTU_VERSION}/pool/stable/amd64/"
export MIRRORED_SUBDIR="download.docker.com/linux/ubuntu/dists/${UBUNTU_VERSION}/pool/stable/amd64"
export REPOS_DIR="/data/repos"
export JAMMY_SUBDIRS="dists/${UBUNTU_VERSION}/docker/binary-amd64"



create_subdirs(){
  mkdir -p ${REPOS_DIR}/${JAMMY_SUBDIRS}
}

mirror_url(){
  cd ${REPOS_DIR}

  # -c, --continue - continues download, where it was paused/canceled
  # -m, --mirror – Makes (among other things) the download recursive.
  # -k, --convert-links – convert all the links (also to stuff like CSS stylesheets) to relative, so it will be suitable for offline viewing.
  # -E, --adjust-extension – Adds suitable extensions to filenames (html or css) depending on their content-type.
  # -p, --page-requisites – Download things like CSS style-sheets and images required to properly display the page offline.
  # -np, --no-parent – When recursing do not ascend to the parent directory. It useful for restricting the download to only a portion of the site.
  
  #wget -cmkEpnp ${URL_2_MIRROR}
  wget -cmnp ${URL_2_MIRROR}
}

rsync_debs(){
  rsync -a ${REPOS_DIR}/${MIRRORED_SUBDIR}/ ${REPOS_DIR}/${JAMMY_SUBDIRS}/
}

create_local_repo(){
  cd ${REPOS_DIR}
  cat << EOF > ${REPOS_DIR}/dists/${UBUNTU_VERSION}/Release
Architectures: amd64 
Components: stable 
Label: Docker CE
Origin: Docker
Suite: ${UBUNTU_VERSION}
EOF

  

  dpkg-scanpackages ${JAMMY_SUBDIRS} > ${JAMMY_SUBDIRS}/Packages 2> /dev/null
  dpkg-scanpackages ${JAMMY_SUBDIRS} | gzip -9c > ${JAMMY_SUBDIRS}/Packages.gz 2> /dev/null
  dpkg-scanpackages ${JAMMY_SUBDIRS} | xz -9 > ${JAMMY_SUBDIRS}/Packages.xz 2> /dev/null

  apt-ftparchive release dists/${UBUNTU_VERSION} >> ${REPOS_DIR}/dists/${UBUNTU_VERSION}/Release

  #echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
  echo "deb [trusted=yes arch=amd64] http://192.168.77.56/ ${UBUNTU_VERSION} docker"

}




create_subdirs
mirror_url
rsync_debs
create_local_repo


exit 0