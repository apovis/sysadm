#!/usr/bin/env sh

# Version: 2024.09.23

# initial repo setup on freebsd and installation of needed pkgs

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
#set -x


# vars
export REPO_DIR="/usr/local/etc/pkg/repos"
export DEFAULT_REPO_CONF="/etc/pkg/FreeBSD.conf"
export PKGS="bash git tig curl rsync python htop neovim"

mkdir -p ${REPO_DIR}
sed 's/quarterly/latest/g' ${DEFAULT_REPO_CONF} > ${REPO_DIR}/FreeBSD.conf
pkg
pkg update
pkg install -y ${PKGS}

exit 0