#!/bin/bash

# Version: 2023.06.20

# a quick script for setting up neovim

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob
#set -x



neovim_root() {
  echo "where is root's home?"
  read ROOT_HOME_DIR
  #ROOT_HOME_DIR="/builds/chroots/bookworm_small/root"
  
  GIT_CHECKOUT_DIR="/tmp/vim"

  git clone https://gitlab.com/apovis/vim.git ${GIT_CHECKOUT_DIR}
  curl -fLo "${ROOT_HOME_DIR}"/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  mkdir -p ${ROOT_HOME_DIR}/.config/nvim
  cp ${GIT_CHECKOUT_DIR}/vimrc ${ROOT_HOME_DIR}/.config/nvim/init.vim 
  #cp -a ${GIT_CHECKOUT_DIR}/autoload ${ROOT_HOME_DIR}/.local/share/nvim/site/

  # cleanup
  rm -rf ${GIT_CHECKOUT_DIR}
}

neovim_user() {
  echo "what is your user's name?"
  read USERNAME
  #USERNAME=""
  echo "where is ${USERNAME}'s home?"
  read USERS_HOME_DIR
  #USERS_HOME_DIR=""
  
  GIT_CHECKOUT_DIR="/tmp/vim"

  git clone https://gitlab.com/apovis/vim.git ${GIT_CHECKOUT_DIR}
  curl -fLo "${USERS_HOME_DIR}"/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  mkdir -p ${USERS_HOME_DIR}/.config/nvim
  cp ${GIT_CHECKOUT_DIR}/vimrc ${USERS_HOME_DIR}/.config/nvim/init.vim 
  #cp -a ${GIT_CHECKOUT_DIR}/autoload ${USERS_HOME_DIR}/.local/share/nvim/site/

  # cleanup
  rm -rf ${GIT_CHECKOUT_DIR}

  chown -R ${USERNAME}:${USERNAME} ${USERS_HOME_DIR}
}



# run functions here
neovim_root
neovim_user
