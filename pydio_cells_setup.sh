#!/bin/bash

# Version: 2023.05.16

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob


# Create pydio user with a home directory
useradd -m -s /bin/bash pydio

# Create necessary folders
mkdir -p /opt/pydio/bin /var/cells
chown -R pydio: /opt/pydio /var/cells

# Add system-wide ENV var
tee -a /etc/profile.d/cells-env.sh << EOF
export CELLS_WORKING_DIR=/var/cells
EOF
chmod 0755 /etc/profile.d/cells-env.sh

# install mariadb
pacman -Sy mariadb

# start db
systemctl start mariadb

# Run the script to secure your install
mysql_secure_installation

# install db
mariadb-install-db --user=mysql --basedir=/usr --datadir=/var/lib/mysql

# start and enable db
systemctl enable --now mariadb

echo "
CREATE DATABASE cells;
CREATE USER 'pydio'@'localhost' IDENTIFIED BY 'ooxJeam4RaSweb6grooHulIlk';
GRANT ALL PRIVILEGES ON cells.* to 'pydio'@'localhost';
FLUSH PRIVILEGES;
exit " >> setup_db.sql

mysql -u root -p << setup_db.sql


# get binary
export distribId=cells 
su -l pydio -c "wget -O /opt/pydio/bin/cells https://download.pydio.com/latest/${distribId}/release/{latest}/linux-amd64/${distribId}"

# Make it executable
chmod 0755 /opt/pydio/bin/cells

# As sysadmin user 
# Add permissions to bind to default HTTP ports
#setcap 'cap_net_bind_service=+ep' /opt/pydio/bin/cells

# Declare the cells commands system wide
ln -s /opt/pydio/bin/cells /usr/local/bin/cells

# test version
echo "testing for pydio's version"
su -l pydio -c "cells version"

exit 0