#!/bin/bash

# Version: 2021.06.09

set -o nounset
set -o errexit
#set -o noclobber
#set -o noglob

eval $(date +"YEAR=%Y MONTH=%m DAY=%d MINUTE=%M HOUR=%H")

CRPTS=$(which cryptsetup)
SUDO=$(which sudo)

if [[ -z "$1" ]]; then
	echo "$0: device"
	echo
	echo "Examples:"
	echo "$0 sdb3"
	echo
	
	exit 1

else

	${SUDO} ${CRPTS} luksHeaderBackup /dev/$1 --header-backup-file /etc/luks.bak/${YEAR}_${MONTH}_${DAY}_-_${HOUR}_${MINUTE}_$1
	
	exit 0

fi